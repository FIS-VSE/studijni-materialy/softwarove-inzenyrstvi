
# Release aplikace {#release}

Release (*vydávání* nebo *vydání*) je široký pojem označující aktivity vedoucí k transformaci **vyvíjené verze aplikace do verze připravené k použití**. Pojem může také označovat právě tu verzi aplikace, která transformací prochází nebo jí již prošla.

Pokud použijeme doporučený postup pro větvení kódu, vyvíjená verze aplikace se nachází v *develop* větvi. Ve chvíli, kdy vyvíjená verze aplikace obsahuje požadovanou funkcionalitu, je spojena s *release* větví. Práce na *release* větvi už nepřidávají žádnou novou hodnotu, ale zvyšují stabilitu aplikace. Verze aplikace v *release* větvi se někdy označuje jako RC (*Release candidate*). Více o strategii práce s větvemi se dozvíte v [tutoriálu](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) od Atlassian.

Po dostatečném otestování může být aplikace uvolněna pro použití a nasazena do provozu. Doporučené je spojit takto připravenou verzi s *master* větví pomocí *merge request* a označit příslušnou záložkou s čitelnou verzí hotové aplikace.

![Graf verzí a release v master větvi (zdroj: NLX projekt na GitLab.com)](images/mergingGraph.png)

Další informace o vydávání aplikace si můžete přečíst v článcích [Release](https://www.atlassian.com/agile/software-development/release) a [Stress-free release](https://www.atlassian.com/agile/software-development/stress-free-release) od Atlassian.

## Pojmenování verze

Ačkoli má každá verze softwaru, která se vyvíjí v systému pro správu verzí, svůj hash, pro orientaci je nutné použít systém číslování, který bude srozumitelný pro vývojáře i uživatele.

V praxi se ukázalo, že nestačí jedno číslo verze, protože popis verze musí nést více informací. Minimálně to, jestli verze obsahuje **novou funkcionalitu** a jestli je dodržena **zpětná kompatibilita** API s předešlými verzemi.

Systém verzování, který obsahuje tyto informace, se nazývá [sémantické verzování](https://semver.org/lang/cs/). Předepisuje, aby verze byla označena v tomto formátu:

 `MAJOR.MINOR.PATCH-meta`

**První číslo (*major*)** označuje významnou změnu, která mění **zpětnou kompatibilitu rozhraní** (*API*) aplikace. V minulosti se ale často jednalo o marketingový tah, který měl vyvolat dojem vyšší vyzrálosti softwaru a potažmo měl za cíl vyrovnat se konkurenci, nebo se jednalo o zevrubnější změny v aplikaci (viz [Political and cultural significance of version numbers](https://en.wikipedia.org/wiki/Software_versioning#Political_and_cultural_significance_of_version_numbers)).

**Druhé číslo (*minor*)** se zvyšuje u všech dalších změn, které zpětnou kompatibilitu zachovávají a přinesly **novou funkcionalitu**.

**Poslední číslo (*patch*)** se zvyšuje při každé změně, která nemění zpětnou kompatibilitu a nepřináší ani novou funkcionalitu. Tyto změny pouze **zvyšují stabilitu aplikace**.

Název verze může být doplněn o **dodatečné informace (*meta*)** za pomlčkou. Nejčastěji se uvádí, že se ještě nejedná o hotový produkt, ale že se stále ještě vyvíjí (např. *SNAPSHOT*, *WIP*, *alpha*, *beta*).

## Tvorba záložek na GitLabu

Seznam záložek (*tags*) se nachází na GitLab.com v menu *Repository > Tags*. Novou záložku je možné přidat kliknutím na zelené tlačítko *New tag*.

![Seznam záložek (zdroj: LeafPic projekt na GitLab.com)](images/tagsGitLab.png)

U nové záložky se vyplňuje **jméno** a **revize, pro kterou se záložka vytváří**. Pokud se jedná o release, je obvyklé použít jméno verze uvozené písmenem *v*, například *v1.0.0*. Označení verze by se mělo držet zmíněných zásad [sémantického verzování](https://semver.org/lang/cs/).

Záložku je možné založit pro jakoukoli revizi v historii projektu, stačí **vyplnit hash revize**. Alternativně je možné napsat jméno větve a automaticky se bere poslední revize ve větvi.

K záložce je možné připojit **zprávu** (*message*) podobně jako ke každé revizi. Zpráva může krátce shrnout podstatu nové verze.

Další informace o verzi v podobě formátovaného textu je možné připojit poznámkou do *Release notes*. Do poznámky je vhodné shrnout všechny změny s krátkým popisem a připojit zkompilovaný otestovaný soubor s aplikací jako přílohu.

## Kontinuální integrace a nasazení

S vydáváním softwaru úzce souvisí kontinuální integrace a nasazení (*Continual Integration / Continual Deployment, zkráceně CI/CD*). Pojem označuje **rutinní aktivity** vedoucí k testování změn aplikace, spojování do hlavních větví a sestavení doručitelného softwaru. Více informací najdete v článku [Continuous integration vs delivery vs deployment](https://www.atlassian.com/continuous-delivery/principles/continuous-integration-vs-delivery-vs-deployment).

Kontinuální integrace a nasazení je závislé na **automatizaci**. Po splnění určitých podmínek se spustí *pipeline*, která ve vybraném *kontejneru* vykoná **sled příkazů**. Jeden běh může být rozdělen do více fází (*job*) podle účelu, např. sestavení, testování a nasazení.

Podmínkou může být buď plánovaná **časová událost** (*[schedule](https://gitlab.com/help/user/project/pipelines/schedules)*), nebo ***commit*** do určité větve. Obvyklé je software sestavovat pravidelně nebo při nové revizi v *master* větvi.

**Kontejner** je balík softwaru, který je vložený do izolovaného prostředí, které obsahuje vše potřebné pro svůj běh. Například pro sestavení Java aplikace vyvíjené jako Maven projekt je zapotřebí kontejner, který obsahuje Java SDK a Maven. Více o kontejnerech se dozvíte v článku [What is a Container?](https://www.docker.com/resources/what-container)

![Přehled sestavení na GitLabu](images/CICDpipelines.png)

Shrnutí nejpoužívanějších řešení pro CI/CD najdete v článku [Best 14 CI/CD Tools You Must Know](https://katalon.com/resources-center/blog/ci-cd-tools/).

Na **GitLabu** se integrace spouští automaticky podle konfigurace v souboru `gitlab-ci.yml`. Soubor se musí nacházet v kořeni repozitáře a mít validní *YAML* syntaxi, jinak nebude brán v úvahu. *YAML* je, podobně jako *XML* nebo *JSON*, syntaxe pro zápis dat v hierarchické struktuře. Soubor musí obsahovat odkaz na kontejner a sled příkazů organizovaných do jednotlivých fází.

Následující příklad konfigurace definuje jednu fázi, *sestavení*. Pod položkou `image` určí, že pro běh se použije kontejner obsahující Java SDK 8 a Maven s názvem `kaiwinter/docker-java8-maven`.

Do fáze `sestaveni` je vnořena položka `skript`, ve které je jediný příkaz, který zahájí testování jednotkovými testy a sestavení spustitelného archivu, `mvn install -B`.

Kromě toho obsahuje fáze `sestaveni` ještě položku `only`, která omezí běh *pipeline* jen na revize ve větvi *master*.

Poslední část se věnuje zachycení artefaktů. Do položky `artifacts` je třeba nakonfigurovat cestu k sestavenému archivu do položky `paths`. Po skončení běhu *pipeline* je možné archiv stáhnout, a to bez nutnosti kontaktovat vývojáře.


```yaml
image: kaiwinter/docker-java8-maven

sestaveni:
 script:
 - "mvn install -B"
 only:
 - master

 artifacts:
    paths:
    - target/adventura-cv-1.0.0.jar
```


Každý běh skriptu skončí buď úspěšně a je označen zeleným tlačítkem *passed*, nebo neúspěšně a je označen červeným *failed*. Po kliknutí na tlačítko se zobrazí podrobný výpis.

Více informací o kontinuální integraci a nasazení najdete v [dokumentaci Gitlab.com](https://docs.gitlab.com/ee/ci/).
