
## Maven projekt s OpenJDK 8 – varianta C {#zalozeni-C}

Podmínky pro uplatnění postupu:

* Je nainstalovaná Java OpenJDK verze 8.

Připojení knihovny JavaFX:

* Jelikož JavaFX není součástí OpenJDK 8 a v Maven repozitáři nejsou JavaFX knihovny pro verzi 8, je nutné knihovny JavaFX do projektu připojit ručně v konfiguraci projektu.

V případě, že vám podmínky neumožňují vytvořit projekt tímto způsobem, prozkoumejte [další varianty](#zalozeni).

### Postup založení

Postup je stejný jako u [*varianty A*](#zalozeni-A) s tím rozdílem, že je nutné mít nainstalovanou knihovnu JavaFX a případně nastavit ručně propojení s projektem.

Podrobnosti o instalaci najdete v kapitole **JavaFX**.

### Propojení JavaFX s projektem

Pokud **IDE neakceptuje třídy knihovny JavaFX**, je to známka buď nepovedené instalace JavaFX, nebo nutnosti propojit projekt s nainstalovanou knihovnou JavaFX na disku.

To je možné napravit tímto způsobem:

1. Otevřete dialog *Project Structure*, který je možné otevřít z nabídky *File > Project Structure*.
1. V dialogu na levé straně otevřete záložku ***Libraries***, která otevře přehled externích knihoven projektu.
1. Klikněte na **zelený symbol plus** nad levým seznamem knihoven a vyberte *Java*.

![Přehled externích knihoven](images/ideaExternalLibraryLibraries.png)

4. V dialogu *Select Library Files* můžete na disku vyhledat umístění nainstalované knihovny JavaFX. Stačí přidat soubor ***jfxrt.jar***. Pokud nevíte přesně, kde soubor hledat, můžete použít příkaz `locate jfxrt.jar` (Linux).

![Výběr souborů knihovny](images/ideaExternalLibraryNewSelectedRt.png)

5. JavaFX by měla být vidět mezi ostatními knihovnami.

![Přehled externích knihoven](images/ideaExternalLibraryLibrariesAdded.png)

6. V projektovém přehledu byste měli knihovnu vidět v sekci *External Libraries*, a to včetně jejího obsahu.

![Přehled externích knihoven](images/ideaExternalLibraryAddedProjectOverview.png)
